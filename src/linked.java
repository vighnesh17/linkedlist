import java.util.Iterator;
import java.util.ListIterator;

public class linked<T> implements Iterable<T>
{

        Node head;
        public class Node{
              T data;
            Node next;
            Node(T val){
                data=val;
                next=null;
            }
        }
        public void insertAtbegin(T val){
            Node newnode=new Node(val);
            if(head==null){
                head= newnode;
            }else{
                newnode.next=head;
                head=newnode;
            }


        }
        public void display(){
            Node temp=head;
            while (temp!=null){
                System.out.print(temp.data+"  ");
                temp=temp.next;
            }

        }
        public void insertAtpos(int pos,T val){
            if(pos==0){
                insertAtbegin(val);
                return;
            }
            Node newnode=new Node(val);
            Node temp=head;
            for(int  i=1;i<pos;i++){//jump to the previous pos of the node
                temp=temp.next;//jump
            if(temp==null){
                System.out.println("invalid pos");
                //throw new IlligalArgumentException("invalid pos");
                return;
            }
            }
            newnode.next=temp.next;
            temp.next=newnode;

        }
        public void deleteAtPos(int pos) {
            if (head == null) {
                throw new IndexOutOfBoundsException("deletion attempted on empty list");

            }
            Node temp = head;
            Node pre = null;
            if (pos == 0) {
                head = head.next;
                return;
            }
            for (int i = 1; i <= pos; i++) {
                pre = temp;//keep track of previous node
                temp = temp.next;//jump
            }
            pre.next = temp.next;//reassaign pointers


        }
        public Iterator<T> iterator(){
            return new Iterator<T>() {
                Node temp=head;
                @Override
                public boolean hasNext() {
                    return temp!=null;
                }

                @Override
                public T next() {
                    T val=temp.data;
                    temp=temp.next;
                    return val;
                }
            };

    }
}